
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mysql.cj.api.jdbc.Statement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

@Path("/wikipedia")
public class Wiki {

	private JSONArray keyword_entries = null;

	public HashMap<String, String> pageid_title = new HashMap<>();

	/**
	 * This method connects to the given database.
	 */
	public Connection ConnectToDatabase(String db_name) {

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			System.out.println("Where is your MySQL JDBC Driver?");
			e.printStackTrace();

		}
		System.out.println("MySQL JDBC Driver Registered!");
		Connection connection = null;

		try {
			connection = DriverManager
					// If we don't add the part after "DBName?", there will be
					// an exception about the time zones. It seems that there is
					// an incompatibility.
					.getConnection(
							"jdbc:mysql://localhost:3306/" + db_name
									+ "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC",
							"root", "");

		} catch (SQLException e) {
			System.out.println("Connecting to " + db_name + " failed ! Check output console");
			e.printStackTrace();
			return connection;
		}

		if (connection != null) {
			System.out.println("Everything is fine with connecting to database " + db_name + " !");
		} else {
			System.out.println("Failed to make connection to database: " + db_name + " !");
		}
		return connection;
	}

	@POST
	@Path("/test")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response ExtractFromDatabase() {

		ArrayList<JSONArray> wikipedia_hierarchy = new ArrayList<>();

		// String topic_file =
		// "G:\\University\\SmarTransfer\\.metadata\\.plugins\\org.eclipse.wst.server.core\\tmp0\\expertfinder\\data\\semantic\\keywords1.txt";
//		String topic_file = "G:\\University\\SmarTransfer\\.metadata\\.plugins\\org.eclipse.wst.server.core\\tmp0\\expertfinder\\data\\semantic\\keywords2.txt";
		 String topic_file =
		 "G:\\University\\SmarTransfer\\.metadata\\.plugins\\org.eclipse.wst.server.core\\tmp0\\expertfinder\\data\\semantic\\keywords3.txt";

		ArrayList<String> topic_titles = ReadSeedKeywords(topic_file);

		Connection con = ConnectToDatabase("dewiki");
		Connection con_pages = ConnectToDatabase("dewiki_pages");
		Connection con_redirect = ConnectToDatabase("dewiki_redirect");

		for (int i = 0; i < topic_titles.size(); i++) {
			int cntr = i + 1;
			System.out.println("keyword number: " + cntr);
			JSONArray one_topic_hierarchy = ExtractUpperCategories(con, con_pages, con_redirect, topic_titles.get(i));
			wikipedia_hierarchy.add(one_topic_hierarchy);
		}

//		wikipedia_hierarchy = RemoveWithoutPageIDs(wikipedia_hierarchy);
		wikipedia_hierarchy = KeepUsefulCategories(wikipedia_hierarchy);

		wikipedia_hierarchy = GetEnPageTitles(wikipedia_hierarchy);
		wikipedia_hierarchy = SetStems(wikipedia_hierarchy);

		System.out.println("new hierarchyy after stemming is: " + wikipedia_hierarchy.toString());

		return Response.status(200).entity("NameOFDataBank").build();
	}

	/**
	 * This method removes all pages in the hierarchy which don't include any
	 * page_id.
	 * 
	 * @param wikipedia_hierarchy
	 * @return
	 */
	private ArrayList<JSONArray> RemoveWithoutPageIDs(ArrayList<JSONArray> wikipedia_hierarchy) {
		return null;
	}

	/**
	 * This method reads the complete wikipedia hierarchy for German language,
	 * finds the page titles in English, adds them to the wikipedia hierarchy
	 * and returns the results.
	 * 
	 * @param wikipedia_hierarchy
	 */
	private ArrayList<JSONArray> GetEnPageTitles(ArrayList<JSONArray> wikipedia_hierarchy) {

		for (int i = 0; i < wikipedia_hierarchy.size(); i++) {
			JSONArray one_array = wikipedia_hierarchy.get(i);

			for (int d = 0; d < one_array.length(); d++) {

				try {
					JSONObject one_object = one_array.getJSONObject(d);
					one_object = GetEnPage(one_object);
					one_array.put(d, one_object);

				} catch (JSONException e) {
					e.printStackTrace();
				}

			}
			wikipedia_hierarchy.set(i, one_array);
		}

		return wikipedia_hierarchy;

	}

	/**
	 * This method gets a JSONObject as the input which includes a German page,
	 * searches for its English version and add the English title as an element
	 * in the obejct.
	 * 
	 * @param one_object
	 *            The iput object which now also includes the English
	 *            translation of the title page.
	 * @return
	 */
	private JSONObject GetEnPage(JSONObject one_object) {

		WikiApi wikiApi = new WikiApi();
		try {
			String page_id_de = one_object.getString("page_id_de");
			String page_title_de = one_object.getString("page_title_de");

			String translated = wikiApi.sendGetTranslation("de", page_id_de);

			JSONObject translated_json = new JSONObject(translated);
			String trns = translated_json.getString("response");
			String found_or_not = translated_json.getString("tr");

			if (found_or_not.equalsIgnoreCase("true")) {

				one_object.put("page_title_en", trns.replace("Category:", ""));

				if (one_object.has("parents")) {

					JSONArray parents = one_object.getJSONArray("parents");

					for (int a = 0; a < parents.length(); a++) {

						GetEnPage(parents.getJSONObject(a));

					}
				} else {
					System.out.println("no parent exists.");

				}
			} // end if
			else {

				one_object.put("page_title_en", page_title_de);

				if (one_object.has("parents")) {

					JSONArray parents = one_object.getJSONArray("parents");

					for (int a = 0; a < parents.length(); a++) {

						GetEnPage(parents.getJSONObject(a));
					}
				}

			} // end else

		} catch (Exception e) {

			e.printStackTrace();
		}
		return one_object;
	}

	/**
	 * This method sets the German stems of the page_titles in the wikipedia
	 * hierarchy
	 * 
	 * @param wikipedia_hierarchy
	 */
	private ArrayList<JSONArray> SetStems(ArrayList<JSONArray> wikipedia_hierarchy) {

		for (int i = 0; i < wikipedia_hierarchy.size(); i++) {
			JSONArray one_array = wikipedia_hierarchy.get(i);

			for (int d = 0; d < one_array.length(); d++) {

				try {
					JSONObject one_object = one_array.getJSONObject(d);
					one_object = GetStem(one_object, "de");
					one_object = GetStem(one_object, "en");
					one_array.put(d, one_object);

				} catch (JSONException e) {
					e.printStackTrace();
				}

			}
			wikipedia_hierarchy.set(i, one_array);
		}

		return wikipedia_hierarchy;
	}

	/**
	 * This method calls the stem() method from the MainStemmer class in
	 * EF_lemmatizer project.
	 * 
	 * @param title
	 * @return
	 */
	private JSONObject GetStem(JSONObject one_object, String language) {

		String stem = "";
		String title;

		try {
			title = one_object.getString("page_title_" + language);

			StringTokenizer st = new StringTokenizer(title, " _-()", true);

			while (st.hasMoreTokens()) {

				String one_token = st.nextToken();

				// Main m = new Main();
				stem += Main.stem(one_token, language);

			}

			one_object.put("stem_" + language, stem);

			if (one_object.has("parents")) {

				JSONArray parents = one_object.getJSONArray("parents");

				for (int a = 0; a < parents.length(); a++) {

					GetStem(parents.getJSONObject(a), language);
				}
			}

			return one_object;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return one_object;

	}

	/**
	 * This method keeps just some of the parent categories and removes the
	 * rest.
	 * 
	 * @param wikipedia_hierarchy
	 * @return
	 */
	private ArrayList<JSONArray> KeepUsefulCategories(ArrayList<JSONArray> wikipedia_hierarchy) {

		for (int d = 0; d < wikipedia_hierarchy.size(); d++) {

			// Hierarchies for one topic
			JSONArray one_topic_hierarchy = wikipedia_hierarchy.get(d);

			wikipedia_hierarchy = StopAtTopLevel(d, one_topic_hierarchy, wikipedia_hierarchy);
		}
		return wikipedia_hierarchy;
	}

	/**
	 * 
	 * @param one_topic_hierarchy
	 * @param wikipedia_hierarchy
	 */
	private ArrayList<JSONArray> StopAtTopLevel(int d, JSONArray one_topic_hierarchy,
			ArrayList<JSONArray> wikipedia_hierarchy) {

		try {
			// First level of the hierarchy contains the topic itself
			JSONObject wiki_one_object = one_topic_hierarchy.getJSONObject(0);
			// Parent of the topic
			JSONArray current_parents = wiki_one_object.getJSONArray("parents");

			for (int t = 0; t < current_parents.length(); t++) {

				// Grandparents of the topic
				JSONObject current_parent = (JSONObject) current_parents.getJSONObject(t);

				String title = current_parent.getString("page_title_de");

				if (IsStopLevel(title, current_parent)) {

					String page_id = current_parent.get("page_id_de").toString();
					String page_title = current_parent.get("page_title_de").toString();

					JSONObject new_current_parent = new JSONObject();
					new_current_parent.put("page_id_de", page_id);
					new_current_parent.put("page_title_de", page_title);

					current_parents.put(t, new_current_parent);
				}

			}

			wiki_one_object.put("parents", current_parents);
			wikipedia_hierarchy.set(d, one_topic_hierarchy);

			return wikipedia_hierarchy;
		} catch (JSONException e) {

			e.printStackTrace();
		}
		return wikipedia_hierarchy;

	}

	/**
	 * This method determines if the level is enough to stop. The words which
	 * show the stopping level are manually determined. They include words such
	 * as: software, program, sprache, windows, web, management.
	 * 
	 * @param title
	 * @param current_parent
	 * @return
	 */
	private boolean IsStopLevel(String title, JSONObject current_parent) {

		if (title.toLowerCase().contains("windows") || title.toLowerCase().contains("sprache")
				|| title.toLowerCase().contains("web") || title.toLowerCase().contains("software")
				|| title.toLowerCase().contains("program") || title.toLowerCase().contains("management")) {
			return true;
		}
		return false;
	}

	/**
	 * This method searches the input word in the database and finds its parents
	 * up to two levels.The method implements a recursive BFS up to two levels
	 * (maxDepth = 1 means level 0 and 1, that is two levels).
	 * 
	 * @param con
	 * @param toSearch
	 * @return
	 */
	private JSONArray ExtractUpperCategories(Connection con, Connection con_pages, Connection con_redirect,
			String wiki_page_title) {

		JSONArray wikipedia_hierarchy = new JSONArray();

		// We get the parents, just up to second level (i.e. parents and
		// grandparents of the given input)
		int maxDepth = 1;
		if (maxDepth < 0) {
			return null;
		}

		ArrayList<String> nodeQueue = new ArrayList<String>();

		System.out.println("wiki page: " + wiki_page_title);
		nodeQueue.add(wiki_page_title);

		int currentDepth = 0, elementsToDepthIncrease = 1, nextElementsToDepthIncrease = 0;

		while (!nodeQueue.isEmpty()) {

			// read the first node and then remove it from the to-see list
			String current = nodeQueue.get(0);
			nodeQueue.remove(0);

			String page_id = GetPageID(current, con_pages, currentDepth);
						
			boolean is_redirected = GetISRedirected(page_id, con_pages, con_redirect);
			if (is_redirected) {
				String redirected_from = GetRedirection(page_id, con_pages, con_redirect);
				current = redirected_from;
				page_id = GetPageID(current, con_pages, currentDepth);
			}

			// Get the parents of the current node (in a tree structure,
			// they are called children!!)
			ArrayList<String> children = process(con, con_pages, current, currentDepth, page_id, wikipedia_hierarchy);

			nextElementsToDepthIncrease += children.size();

			if (--elementsToDepthIncrease == 0) {

				if (++currentDepth > maxDepth) {

					break;
				}

				elementsToDepthIncrease = nextElementsToDepthIncrease;
				nextElementsToDepthIncrease = 0;

			}
			for (int i = 0; i < children.size(); i++) {
				nodeQueue.add(children.get(i));
			}
			
		} // end while

		boolean one_elem_deleted = false;
		for (int i = 0; i < wikipedia_hierarchy.length(); i++) {

			if (i > 0) {

				wikipedia_hierarchy = remove(i, wikipedia_hierarchy);
				one_elem_deleted = true;

				if (one_elem_deleted = true) {
					i--;
				}
			}
		}

		return wikipedia_hierarchy;
	}

	/**
	 * This method determines if a page is redirected from another page in Wiki
	 * 
	 * @param page_id
	 * @param con_pages
	 * @param con_redirect
	 * @return
	 */
	private boolean GetISRedirected(String page_id, Connection con_pages, Connection con_redirect) {

		String query2 = "";
		try {

			query2 = "SELECT * FROM page where page_id = '" + page_id + "' && page_namespace <> '14'";

			Statement st2 = (Statement) con_pages.createStatement();
			ResultSet rs2 = st2.executeQuery(query2);

			while (rs2.next()) {

				String page_is_redirected = rs2.getString("page_is_redirect");
				if (page_is_redirected.equalsIgnoreCase("1")) {
					return true;
				} else
					return false;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Since there is no Remove() method for the JSONArray in java, we had to
	 * develop it ourselves. This code is taken from StackOverFlow
	 * 
	 * @param idx:
	 *            The index to remove
	 * @param from:
	 *            The JSONArray to remove the element from
	 * @return
	 */
	public static JSONArray remove(final int idx, final JSONArray from) {

		final List<JSONObject> objs = asList(from);

		objs.remove(idx);

		final JSONArray ja = new JSONArray();
		for (final JSONObject obj : objs) {
			ja.put(obj);
		}

		return ja;
	}

	/**
	 * To remove the element from the JSONArray (ja), we need to convert it to a
	 * list of JSONObject
	 * 
	 * @param ja
	 * @return
	 */
	public static List<JSONObject> asList(final JSONArray ja) {
		final int len = ja.length();
		final ArrayList<JSONObject> result = new ArrayList<JSONObject>(len);
		for (int i = 0; i < len; i++) {
			final JSONObject obj = ja.optJSONObject(i);
			if (obj != null) {
				result.add(obj);
			}
		}
		return result;
	}

	/**
	 * This method does the main process in the data base. That is, it runs the
	 * queries and get the parents of the given input in the Wikipedia database.
	 * 
	 * @param con:
	 *            connector to the deutsch "categorylinks" wikipedia database
	 * @param con_pages
	 *            : connector to the deutsch page_map wikipedia database
	 * @param node
	 *            : The given wikipedia_page/category to find its parents.
	 * @param current_step:
	 *            whether we are the 0 level or more. We need it to determine if
	 *            we look for a category or page in wikipedia database.
	 * @return : The arraylist of the found parents.
	 */
	public ArrayList<String> process(Connection con, Connection con_pages, String node, int current_step,
			String page_id, JSONArray wikipedia_hierarchy) {

		ArrayList<String> children = new ArrayList<>();
		try {

			JSONObject one_page = new JSONObject();

			JSONArray arr = new JSONArray();
			one_page.put("page_title_de", node);
			one_page.put("page_id_de", page_id);

			String query = "SELECT * FROM categorylinks where cl_from = '" + page_id + "'";
			Statement st = (Statement) con.createStatement();
			ResultSet rs = st.executeQuery(query);

			while (rs.next()) {

				String cl_to = rs.getString("cl_to");

				if (IsRelevant(cl_to)) {

//					System.out.println("cl_to is: " + cl_to);
					// Some categories are things like "wikipedia...x... pages"
					// which should be removed from the list of parents.
					String cl_to_id = GetPageIDChildren(cl_to, con_pages, current_step);

					children.add(cl_to);

					JSONObject child = new JSONObject();
					child.put("page_id_de", cl_to_id);
					child.put("page_title_de", cl_to);

//					This part is to avoid some exceptional cases. They have a page_title, but no page_id
					if (cl_to_id.equalsIgnoreCase("") == false) {						
						arr.put(child);
					}
				}
			}

			one_page.put("parents", arr);

			wikipedia_hierarchy.put(one_page);

			if (current_step != 0) {

				SearchAndMergeInParents(one_page, wikipedia_hierarchy);
			}

		} catch (Exception ex) {
			ex.printStackTrace();

		}
		return children;
	}

	/**
	 * This method determines if the extracted category is meaningful. The
	 * nonsense categories are categories which: 1) start with
	 * "Wikipedia..._pages" 2)"Abkuerzung" 3) follow the format X_nach_Y (e.g.
	 * Informatik_nach_Fachgebiet)
	 * 
	 * @param cl_to
	 * @return
	 */
	private boolean IsRelevant(String cl_to) {

		if (cl_to.startsWith("Wikipedia") == false && cl_to.contains("rzung") == false
				&& cl_to.contains("_nach_") == false && cl_to.contains("_als_") == false
				&& cl_to.contains("benutzer:") == false) {
			return true;
		}
		return false;
	}

	private String GetRedirection(String page_id, Connection con_pages, Connection con_redirect) {

		String original_title = "";
		String query = "";
		try {
			query = "SELECT * FROM redirect where rd_from = '" + page_id + "'";
			Statement st2 = (Statement) con_redirect.createStatement();
			ResultSet rs2 = st2.executeQuery(query);

			while (rs2.next()) {

				original_title = rs2.getString("rd_title");
				return original_title;
			}
			return original_title;
		} catch (SQLException e) {
			e.printStackTrace();
			return "";
		}

	}

	/**
	 * This method get the pageid of the parent of a page. We develop a specific
	 * method for this purpose and don't use the GetPageID, because in
	 * Wikipedia, page ids are different from category ids. For example, the
	 * page "online-marketing" has a different id from the category
	 * "online-marketing".
	 * 
	 * @param cl_to
	 * @param con_pages
	 * @param current_step
	 * @return
	 */
	private String GetPageIDChildren(String cl_to, Connection con_pages, int current_step) {

		String query2 = "";
		try {
			query2 = "SELECT * FROM page where page_title = '" + cl_to + "' && page_namespace = '14'";

			Statement st2 = (Statement) con_pages.createStatement();
			ResultSet rs2 = st2.executeQuery(query2);

			while (rs2.next()) {

				String page_id = rs2.getString("page_id");
				return page_id;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return "";
	}

	/**
	 * When we find the parents and grandparents of a page, the method also adds
	 * the parents and grandparents to the main JSONArray which includes the
	 * page itself. With this method, we add the grandparents as parents' parent
	 * and remove the separate item which includes the grandparents.
	 * 
	 * @param one_page
	 * @throws JSONException
	 */
	private void SearchAndMergeInParents(JSONObject one_page, JSONArray wikipedia_hierarchy) throws JSONException {

		for (int i = 0; i < wikipedia_hierarchy.length(); i++) {
			try {
				JSONObject wiki_one_object = wikipedia_hierarchy.getJSONObject(i);
				JSONArray current_parents = wiki_one_object.getJSONArray("parents");

				for (int d = 0; d < current_parents.length(); d++) {

					JSONObject current_parent = (JSONObject) current_parents.getJSONObject(d);
					if (current_parent.getString("page_id_de").equals(one_page.getString("page_id_de"))) {
						current_parent.put("parents", one_page.getJSONArray("parents"));

					}
				}
			} catch (JSONException e) {

				e.printStackTrace();
			}
		}
	}

	/**
	 * This method gets a string as the input, searches it in the page_id map of
	 * wikipedia and return its page id
	 * 
	 * @param toSearch
	 * @param con_pages
	 * @return
	 */
	private String GetPageID(String toSearch, Connection con_pages, int current_step) {

		String query2 = "";
		try {

			if (current_step == 0) {
				query2 = "SELECT * FROM page where page_title = '" + toSearch
						+ "' && page_namespace <> '14' && page_namespace <> '1'";
			} else {
				// Namespace 14 in wikipedia shows that it is a category
				query2 = "SELECT * FROM page where page_title = '" + toSearch + "' && page_namespace = '14'";
			}
			Statement st2 = (Statement) con_pages.createStatement();
			ResultSet rs2 = st2.executeQuery(query2);

			while (rs2.next()) {

				String page_id = rs2.getString("page_id");
				return page_id;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return "";
	}

	/**
	 * This method reads the seed topics and return them as an arraylist.
	 * 
	 * @param topic_file
	 */
	private ArrayList<String> ReadSeedKeywords(String topic_file) {

		ArrayList<String> de_topics = new ArrayList<String>();
		try {
			JSONObject json = new JSONObject(loadFile(topic_file));

			keyword_entries = json.getJSONArray("data");

			for (int d = 0; d < keyword_entries.length(); d++) {

				JSONObject keyword = keyword_entries.getJSONObject(d);

				String raw_keyword = keyword.getString("name");
				String processed_keyword = raw_keyword.replaceAll(" ", "_");
				de_topics.add(processed_keyword);
			}

			return de_topics;
		} catch (JSONException ex) {

			ex.printStackTrace();
			return de_topics;
		}
	}

	/**
	 * Load a string from a simple text file
	 * 
	 * @param path
	 * @return
	 */
	public static String loadFile(String path)

	{
		String text = "";
		String str = "";
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(path), "UTF-8"));
			while ((str = br.readLine()) != null) {
				text += str.trim() + "\n";
			}

			br.close();

		} catch (Exception e) {
		}
		text = text.substring(0, text.length() - 1);
		return text;
	}

}
