
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Mehmet WikiApi is used to get all entries related to a given category
 *         in english and german.
 */

@Path("/wikiapi")
public class WikiApi {

	private final String USER_AGENT = "Mozilla/5.0";
	static String username = System.getProperty("user.name");
	// private static String path = System.getProperty("catalina.base") +
	// "/expertfinder/data/semantic/";

	@POST
	@Path("/getArticles")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	/**
	 * This method calls the Wikipedia API to get every related entry to a given
	 * category and translate the entries if possible
	 * 
	 * @params data includes the path to load the jsonfile from and the path to
	 *         safe the file to
	 * @return simple String "built lemmata"
	 */
	public static Response getArticles(String data) {
		String lang = "";
		String request_string = "";
		String trLang;
		String jsonWikiPath = data.split(";")[0];
		String path = data.split(";")[1];
		String newFileName = "";
		JSONObject jsonWiki = new JSONObject();
		JSONObject categories;
		JSONArray wiki_pages = new JSONArray();
		String values = "";

		// System.out.println(path);
		try {
			System.out.println(jsonWikiPath);
			jsonWiki = new JSONObject(loadFile(jsonWikiPath));
			System.out.println("jsonWIKI: " + jsonWiki);
			// System.out.println("pages"+jsonWiki.getString("wiki_pages"));
			// System.out.println("titles"+jsonWiki.getString("category_title"));

			categories = new JSONObject(jsonWiki.getString("category_title"));
			newFileName = "output" + categories.getString("title_de").replaceAll(" ", "_");
			wiki_pages = new JSONArray(jsonWiki.getString("wiki_pages"));
			System.out.println("titles: " + wiki_pages);

			String response = "";
			String trValue = "";

			JSONArray array = new JSONArray();
			JSONObject responseJson = new JSONObject();
			WikiApi http = new WikiApi();
			HashMap<String, String> pagesHash = new HashMap<String, String>();

			// run through all given wiki pages and extract their entries
			for (int obj = 0; obj < wiki_pages.length(); obj++) {
				lang = wiki_pages.getJSONObject(obj).getString("lang");
				request_string = wiki_pages.getJSONObject(obj).getString("title");
				String newTitleDe = "";
				String newTitleEn = "";

				if (wiki_pages.getJSONObject(obj).has("new_title_de"))
					newTitleDe = wiki_pages.getJSONObject(obj).getString("new_title_de");
				else
					newTitleDe = request_string;

				if (wiki_pages.getJSONObject(obj).has("new_title_en"))
					newTitleEn = wiki_pages.getJSONObject(obj).getString("new_title_en");
				else
					newTitleEn = newTitleDe;

				if (lang.equals("en")) {
					trLang = "de";
				} else {
					trLang = "en";
				}
				try {
					response = http.sendGetList(lang, request_string);
					// System.out.println("Response: " + response);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				try {
					responseJson = new JSONObject(response);
					array = responseJson.getJSONObject("query").getJSONArray("categorymembers");
					// start array for first object
					if (obj == 0)
						values += "[";

					// start json object
					values += "{\"title\":{" + "\"title_en\":\"" + newTitleEn + "\",\"title_de\":\"" + newTitleDe
							+ "\"},";
					values += "\"entries\":[";

					// iterate through all page entries in this category
					for (int i = 0; i < array.length(); i++) {
						JSONObject entry = new JSONObject();
						entry = array.getJSONObject(i);

						if (entry.getInt("ns") == 0) // only extract articles
														// pages, no categories
						{
							// get next title
							String next = entry.getString("title");
							if (!pagesHash.containsKey(next)) {
								pagesHash.put(next, next);

								// if there's a category in brackets, e.g. Java
								// (programming language), delete it
								next = next.replaceAll(" \\(.+\\)", "");
								values += "{\"pageid\":\"" + entry.getString("pageid") + "\",";
								values += "\"title_" + lang + "\":\"" + next + "\",";
								trValue = http.sendGetTranslation(lang, entry.getString("pageid"));
								trValue = trValue.replaceAll(" \\(.+\\)", "");
								values += "\"title_" + trLang + "\":\"" + trValue + "\"}";

								if (i < array.length() - 1)
									values += ",";
							}
						}

					}
					values += "]}"; // close json array and json object
					// entries += values+",";
					// values += ",";

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				// close array or add comma for next object
				if (obj == wiki_pages.length() - 1)
					values += "]";
				else
					values += ",";
			}

		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// save file
		saveToFile(values, path + newFileName + ".txt");

		// Add stems to entries
		restFullCall("http://localhost:8080/EF_Lemmatizer/lemmatizer/lemmatizeJSON", path + newFileName + ".txt");

		// writeToFile(entries,path + newFileName + ".txt");
		return Response.status(200).entity("Built lemmata").build();
	}

	/*
	 * @POST
	 * 
	 * @Path("/getArticles2")
	 * 
	 * @Consumes(MediaType.MULTIPART_FORM_DATA) public static Response
	 * getArticles2(String data){ String lang = ""; String request_string ="";
	 * String trLang; String jsonWikiPath = data.split(";")[0]; String path =
	 * data.split(";")[1]; String newFileName=""; JSONObject jsonWiki = new
	 * JSONObject(); JSONObject categories; JSONArray wiki_pages = new
	 * JSONArray(); String entries = ""; System.out.println(path); try {
	 * System.out.println(jsonWikiPath); jsonWiki = new
	 * JSONObject(loadFile(jsonWikiPath)); System.out.println("jsonWIKI: "
	 * +jsonWiki);
	 * //System.out.println("pages"+jsonWiki.getString("wiki_pages"));
	 * //System.out.println("titles"+jsonWiki.getString("category_title"));
	 * 
	 * categories = new JSONObject(jsonWiki.getString("category_title"));
	 * newFileName = "output" + categories.getString("title_de").replaceAll(" ",
	 * "_"); wiki_pages = new JSONArray(jsonWiki.getString("wiki_pages"));
	 * System.out.println("titles: "+wiki_pages);
	 * 
	 * String response = ""; String trValue=""; String values ="";
	 * 
	 * JSONArray array = new JSONArray(); JSONObject responseJson = new
	 * JSONObject(); WikiApi http = new WikiApi(); HashMap<String, String>
	 * pagesHash = new HashMap<String, String>(); for(int obj =0;
	 * obj<wiki_pages.length();obj++){ lang =
	 * wiki_pages.getJSONObject(obj).getString("lang"); request_string =
	 * wiki_pages.getJSONObject(obj).getString("title"); if(lang.equals("en")){
	 * trLang ="de"; } else { trLang ="en"; } try { response =
	 * http.sendGetList(lang,request_string); System.out.println("Response: " +
	 * response); } catch (Exception e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); }
	 * 
	 * try { responseJson= new JSONObject(response); array =
	 * responseJson.getJSONObject("query").getJSONArray("categorymembers");
	 * if(obj ==0){ //System.out.println("RESPONSE: "+responseJson);
	 * //System.out.println(array.toString()); values+="{\"category_title\":{";
	 * values+="\"title_en\":\"" + categories.getString("title_en") +"\",";
	 * values+="\"title_de\":\"" + categories.getString("title_de") +"\"";
	 * values += "},"; values+="\"entries\":["; }
	 * 
	 * for (int i=0; i<array.length(); i++){
	 * 
	 * JSONObject entry = new JSONObject(); entry = array.getJSONObject(i);
	 * 
	 * if(entry.getInt("ns") == 0) // only extract articles pages, no categories
	 * { // get next title String next = entry.getString("title");
	 * if(!pagesHash.containsKey(next)){ pagesHash.put(next,next);
	 * 
	 * // if there's a category in brackets, e.g. Java (programming language),
	 * delete it next = next.replaceAll(" \\(.+\\)", "");
	 * values+="{\"pageid\":\"" + entry.getString("pageid") +"\",";
	 * values+="\"title_"+lang+"\":\"" + next +"\","; trValue =
	 * http.sendGetTranslation(lang,entry.getString("pageid")); trValue =
	 * trValue.replaceAll(" \\(.+\\)", ""); values+="\"title_"+trLang+"\":\"" +
	 * trValue +"\"},";
	 * 
	 * } }
	 * 
	 * 
	 * } //entries += values+","; //values += ",";
	 * 
	 * } catch (JSONException e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); } catch (Exception e) { // TODO Auto-generated catch
	 * block e.printStackTrace(); }
	 * 
	 * } if(values.charAt(values.length()-1)==','){ entries +=
	 * values.substring(0,(values.length()-1))+"]}"; System.out.println(
	 * "KOMMA: "+entries); } else { entries += values+"]}"; System.out.println(
	 * "KEIN KOMMA: "+entries); }
	 * 
	 * 
	 * 
	 * } catch (JSONException e1) { // TODO Auto-generated catch block
	 * e1.printStackTrace(); }
	 * 
	 * // save file saveToFile(entries, path + newFileName + ".txt");
	 * 
	 * // Add stems to entries restFullCall(
	 * "http://localhost:8080/EF_Lemmatizer/lemmatizer/lemmatizeJSON", path +
	 * newFileName + ".txt");
	 * 
	 * //writeToFile(entries,path + newFileName + ".txt"); return
	 * Response.status(200).entity("Built lemmata").build(); }
	 * 
	 */
	/**
	 * @param lang
	 *            request language (de,en)
	 * @param categorie
	 * @return all entries related to the categorie
	 * @throws Exception
	 */
	private String sendGetList(String lang, String categorie) throws Exception {
		String url = "https://" + lang + ".wikipedia.org/w/api.php"
				+ "?action=query&list=categorymembers&cmtitle=Category:" + categorie + "&format=json&cmlimit=500";

		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// optional default is GET
		con.setRequestMethod("GET");

		// add request header
		con.setRequestProperty("User-Agent", USER_AGENT);

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'GET' request to URL : " + url);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		// print result
		// System.out.println(response.toString());
		return response.toString();

	}

	public String sendGetTranslation(String lang, String pageid) throws Exception {

//		
//		System.out.println(lang);
//		System.out.println(pageid);
		/**
		 * tries to translate a given entry from german to english or from
		 * english to german if possible and returns a json object with the
		 * translated entry and a boolean to see if it could be translated
		 * 
		 * @param lang
		 *            request language (de,en)
		 * @param pageId
		 *            pageId to the entry which will be translated if possible
		 * @return json object with the translated response and a boolean (tr)
		 *         to see if it could be translated
		 * @throws Exception
		 */

		String trLangs;
		if (lang.equals("en")) {
			trLangs = "de";
		} else {
			trLangs = "en";
		}
		String url = "https://" + lang + ".wikipedia.org/w/api.php?action=query&format=json" + "&prop=langlinks&lllang="
				+ trLangs + "&pageids=" + pageid;
		
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// optional default is GET
		con.setRequestMethod("GET");

		// add request header
		con.setRequestProperty("User-Agent", USER_AGENT);

		int responseCode = con.getResponseCode();
		// System.out.println("\nSending 'GET' request to URL : " + url);
		// System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		System.out.println("ANTWORT" + response);
		JSONObject jsonObject = new JSONObject(response.toString());
		
		String translatedResponse = "";
		JSONObject JsonResponse = new JSONObject();
		boolean tr = false;
		if (jsonObject.getJSONObject("query").getJSONObject("pages").getJSONObject(pageid).has("langlinks")) {
			translatedResponse = jsonObject.getJSONObject("query").getJSONObject("pages").getJSONObject(pageid)
					.getJSONArray("langlinks").getJSONObject(0).getString("*");
			tr = true;
			// System.out.println("Translated: "+translatedResponse);
		} else {
			translatedResponse = jsonObject.getJSONObject("query").getJSONObject("pages").getJSONObject(pageid)
					.getString("title");
			// System.out.println("Not Translated: "+translatedResponse);
			tr = false;
		}
		String temp = "{tr:" + tr + ",response:\"" + translatedResponse + "\"}";
//		System.out.println(temp);
		JsonResponse = new JSONObject(temp);
		 
		return JsonResponse.toString();

	}

	/**
	 * simple method to load a file and return the content
	 * 
	 * @param path
	 *            filepath
	 * @return file content
	 */
	public static String loadFile(String path) {
		String text = "";
		String str = "";

		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(path), "UTF-8"));
			while ((str = br.readLine()) != null) {
				text += str.trim() + "\n";
			}
			br.close();
		} catch (Exception e) {
		}

		text = text.substring(0, text.length() - 1);
		System.out.println(path);
		System.out.println(text);
		return text;
	}

	/**
	 * simple method to safe a file
	 * 
	 * @param text
	 *            content to be safed
	 * @param filename
	 *            name of the file to be saved
	 * @return return the filename
	 */
	public static String saveToFile(String text, String filename) {
		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filename), "UTF-8"));
			writer.write(text);
		} catch (IOException e) {
		} finally {
			try {
				if (writer != null)
					writer.close();
			} catch (IOException e) {
			}
		}
		return filename;
	}

	/**
	 * method to make a http call
	 * 
	 * @param destionationURL
	 *            URL which will be called
	 * @param data
	 *            the data which will be send
	 * @return return the response
	 */
	private static String restFullCall(String destionationURL, String data) {
		String extractedString = "";
		try {
			URL URL = new URL(destionationURL);
			URLConnection connection = URL.openConnection();
			connection.setDoOutput(true);
			connection.setRequestProperty("Content-Type", "multipart/form-data");
			connection.setConnectTimeout(30000);
			connection.setReadTimeout(300000);
			OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
			out.write(data);
			// out.write(json.toString());
			// out.write(jsonObject.toString());
			out.close();

			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

			String line_out;

			while ((line_out = in.readLine()) != null) {
				// System.out.println("Respone: "+line_out);
				extractedString += line_out;
			}

			in.close();

		} catch (Exception e) {
			System.out.println("\nError while calling" + destionationURL);
			System.out.println(e);
		}

		return extractedString;
	}
}
